package com.team13.se;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import com.team13.se.entity.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.team13.se.repository.TicketRepository;

@SpringBootApplication
public class SeApplication {

	@Autowired
	private TicketRepository ticketRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SeApplication.class, args);
	}
	 
	@PostConstruct
	public void setupDbWithData() throws SQLException {

	Connection conn = DriverManager.getConnection("jdbc:h2:file:./data/officeQueue", "sa", "password");
	conn.close();
	}
}
