package com.team13.se.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String index() {
		return book();
	}
	
	@RequestMapping("/bookTicket")
	public String book() {
		return "bookTicket";
	}
	@RequestMapping("/ticketInfo")
	public String showTicket() {
		return "ticketInfo"; 
	}
}
     