package com.team13.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.team13.se.dto.TicketDto;
import com.team13.se.service.TicketService;
import com.team13.se.service.impl.TicketServiceImpl;
import com.team13.se.utils.Constants;

@RequestMapping("/ticket")
@RestController
public class TicketController {
	
	@Autowired
	TicketService ticketService;
	
	//@RequestMapping(Constants.ISSUE_TICKET)
	//serve metodo per creare un nuovo ticket!!
	@RequestMapping(value= Constants.ISSUE_TICKET, method = RequestMethod.POST)
	public boolean newTicket(@RequestBody TicketDto ticketDto) {		
		return ticketService.queue(ticketDto);
	}
	@RequestMapping(value = Constants.GET_LAST_TICKET, method = RequestMethod.GET)
	public TicketDto getLastTicket() {
		return ticketService.getLastTicket();
	} 
	 
}   
