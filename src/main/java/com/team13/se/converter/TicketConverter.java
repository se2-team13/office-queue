package com.team13.se.converter;

import com.team13.se.dto.TicketDto;
import com.team13.se.entity.Ticket;

public class TicketConverter {
	public static TicketDto toTicketDto(Ticket ticket) {
		TicketDto ticketDto = new TicketDto(
				ticket.getIdticket(),
				ticket.getTimestamp(),
				ticket.getRequesttype(),
				ticket.getWaitingtime(),
				ticket.getnInLine()
				);
		return ticketDto;
	}
	
	public static Ticket toTicket(TicketDto ticketDto) {
		Ticket ticket = new Ticket(
				ticketDto.getIdticket(),
				ticketDto.getTimestamp(),
				ticketDto.getRequesttype(),
				ticketDto.getWaitingtime(),
				ticketDto.getnInLine()
				);
		return ticket;
	}
}
