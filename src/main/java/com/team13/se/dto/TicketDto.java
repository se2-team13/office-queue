package com.team13.se.dto;


import com.team13.se.entity.Request;

public class TicketDto {
	 
		private Integer ticketId;
		private String timestamp;
		private Integer requesttype; 
		private Float waitingtime; 
		private Integer nInLine;
		public Integer getnInLine() {
			return nInLine;
		}
		public void setnInLine(Integer nInLine) {
			this.nInLine = nInLine;
		}
		public TicketDto(Integer ticketId, String timestamp, Integer requesttype, Float waitingtime,Integer nInLine) {
			super();
			this.ticketId = ticketId;
			this.timestamp = timestamp;
			this.requesttype = requesttype;
			this.waitingtime = waitingtime;
			this.nInLine = nInLine;
		}
		public Integer getIdticket() {
			return ticketId;
		}
		public void setIdticket(Integer ticketId) {
			this.ticketId = ticketId;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		public Integer getRequesttype() {
			return requesttype;
		}
		public void setRequesttype(Integer requesttype) {
			this.requesttype = requesttype;
		}
		public Float getWaitingtime() {
			return waitingtime;
		}
		public void setWaitingtime(Float waitingtime) {
			this.waitingtime = waitingtime;
		} 
}
