package com.team13.se.entity;

import javax.persistence.*;
import java.util.ArrayList;

@Entity

public class Counter {

    @Id    
    private int id;
    private ArrayList<String> reqTypes = new ArrayList<String> ();  
   
    public Counter() {
    }

    public Counter(int id) {
        this.id = id;
    }

    public void addType(String reqType) {
        reqTypes.add(reqType);
    }

    public ArrayList<String> getTypes() {
        return reqTypes;
    }


    
}