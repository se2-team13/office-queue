package com.team13.se.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import com.team13.se.entity.Ticket;
@Entity
public class Request {
	@Id
	private Integer id;
	@Column
	private String description;
	@Column
	private int avgTime;
	@OneToMany
	List<Ticket> queue = new ArrayList<>();
	private Integer dailyCounter;
	
	public Request(Integer id, String description, int avgTime) {
		super();
		this.id = id;
		this.description = description;
		this.avgTime = avgTime;
		dailyCounter = 0;
	}
	
	public Integer getDailyCounter() {
		return dailyCounter;
	} 

	public void setDailyCounter(Integer dailyCounter) {
		this.dailyCounter = dailyCounter;
	}

	public void incCounter() {
		dailyCounter++;
	}
	
	public void addTicket(Ticket t){
		queue.add(t);
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getAvgTime() {
		return avgTime;
	}
	public void setAvgTime(int avgTime) {
		this.avgTime = avgTime;
	}
	public int getqueuelength() {
		return queue.size();
	}
}
