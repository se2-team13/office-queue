package com.team13.se.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.team13.se.entity.Request;
import lombok.AllArgsConstructor;

import lombok.Builder;

import lombok.Data;

import lombok.NoArgsConstructor;

@AllArgsConstructor
//@NoArgsConstructor
@Builder
@Data
@Entity
public class Ticket implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
    private Integer idticket;
    @Column
    private String timestamp;
    @Column
    private Integer requesttype;
    @Column
    private Integer servicetime;
    @Column
    private Float waitingtime;
    private Integer nInLine;

	public Ticket() {}
    
    public Ticket(Integer idticket, String time, Integer requesttype,Float waitingtime,Integer nInLine ) {
    	this.idticket = idticket;
    	this.timestamp = time;
    	this.requesttype = requesttype;
    	this.waitingtime = waitingtime;
    	this.nInLine = nInLine;
    }
    public Integer getnInLine() {
    	return nInLine;
    }
    
    public void setnInLine(Integer nInLine) {
    	this.nInLine = nInLine;
    }

    public int getIdticket() {
        return idticket;
    }

    public void setIdticket(Integer idticket) {
        this.idticket = idticket;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getRequesttype() {
        return requesttype;   
    }

    public void setRequesttype(Integer requesttype) {
        this.requesttype = requesttype;
    }

    public Float getWaitingtime() {
        return waitingtime;
    }

    public void setWaitingtime(Float waitingtime) {
        this.waitingtime = waitingtime;
    }
}
