package com.team13.se.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.team13.se.entity.Ticket;


@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer>{

}
