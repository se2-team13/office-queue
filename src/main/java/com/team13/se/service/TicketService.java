package com.team13.se.service;

import java.util.List;

import com.team13.se.dto.TicketDto;

public interface TicketService {
	
	List<TicketDto> getAllTickets();
	TicketDto getTicket (Integer id);
    List<TicketDto> getTicketsbyRequestType (String requesttype);
    boolean queue(TicketDto t);
    boolean enqueue();

    TicketDto getLastTicket();

    Float getWaitingTime (Integer requesttype);

}
