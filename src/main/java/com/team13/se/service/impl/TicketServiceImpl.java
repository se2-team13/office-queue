package com.team13.se.service.impl;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.team13.se.entity.Request;
import com.team13.se.entity.Counter;
import com.team13.se.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team13.se.converter.TicketConverter;
import com.team13.se.dto.TicketDto;
import com.team13.se.entity.Ticket;
import com.team13.se.repository.TicketRepository;
import com.team13.se.service.TicketService;

@Service
public class TicketServiceImpl implements TicketService{
	
	@Autowired
	TicketRepository ticketRepository;
	
	
	ArrayList<Request> requestList = new ArrayList<>();
	ArrayList<Counter> counterList = new ArrayList<>();
	TicketDto lastTicket;
	
	public TicketServiceImpl(){
		requestList.add(0,new Request(0,"shipping",900));
		requestList.add(1,new Request(1,"account management",1800));
		requestList.add(2,new Request(2,"pay fees",900));
		requestList.add(3,new Request(3,"pay fines",900));

		counterList.add(0,new Counter(0));
		counterList.get(0).addType("shipping");
		counterList.get(0).addType("pay fees");
		counterList.add(1,new Counter(1));
		counterList.get(1).addType("shipping");
		counterList.get(1).addType("account management");
		counterList.add(2,new Counter(2));
		counterList.get(2).addType("shipping");
		counterList.get(2).addType("pay fees");
		counterList.get(2).addType("pay fines");


	}
	
	@Override
	public List<TicketDto> getAllTickets(){
		List<TicketDto> ticketDtos = new ArrayList<TicketDto>();
		for(Ticket t : ticketRepository.findAll()) {
			ticketDtos.add(TicketConverter.toTicketDto(t));
		}
		return ticketDtos;
	}
	@Override
	public TicketDto getTicket (Integer id) {
		Ticket ticket = ticketRepository.getOne(id);
		return TicketConverter.toTicketDto(ticket);
	}
	@Override
	public List<TicketDto> getTicketsbyRequestType (String requesttype){
		List<TicketDto> ticketDtos = new ArrayList<TicketDto>();
		for(Ticket t : ticketRepository.findAll()) {
			if(t.getRequesttype().equals(requesttype))
				ticketDtos.add(TicketConverter.toTicketDto(t));
		}
		return ticketDtos;
	}
	@Override
	public TicketDto getLastTicket() {
		int max=-1; 
		System.out.println(ticketRepository.findAll());
		for (Ticket t: ticketRepository.findAll()) {
			if(t.getIdticket() > max){
				max = t.getIdticket();
			}
		}
		Optional<Ticket> ticket = ticketRepository.findById(max);
		if(ticket.isPresent()){
			//return TicketConverter.toTicketDto(ticket.get());
			return lastTicket;
		}
		return null;
	}
	@Override
	public boolean queue(TicketDto t) {		
		Request type = requestList.get(t.getRequesttype()) ;
		Optional<Ticket> optId = ticketRepository.findAll().stream().max((a,b)-> a.getIdticket() - b.getIdticket());
		if(optId.isPresent()){
			t.setIdticket(optId.get().getIdticket()+1);
		}else{
			t.setIdticket(0);
		}
		Timestamp time = new Timestamp((System.currentTimeMillis()/1000)*1000);
		t.setTimestamp(time.toString());
		t.setWaitingtime(getWaitingTime(t.getRequesttype()));
		t.setnInLine(type.getDailyCounter());
		//se da fare qui
		lastTicket = t;
		type.addTicket(TicketConverter.toTicket(t));
		type.incCounter();
		ticketRepository.save(TicketConverter.toTicket(t));
		ticketRepository.flush();
		return true;
	}
	
	@Override
	public  boolean enqueue() {
		return true;
	}

	@Override
	public Float getWaitingTime (Integer requesttype) {
		int avgTime = requestList.get(requesttype).getAvgTime();
		int queuelength = requestList.get(requesttype).getqueuelength();
		String requestname = requestList.get(requesttype).getDescription();
		float waitTime = 0;
		float sum = 0;
		
		System.out.println(counterList.size());
		for (int i =0;i <counterList.size(); i++) {
			if (counterList.get(i).getTypes().contains(requestname)) {
				
				sum += (1/(float)(counterList.get(i).getTypes().size()));
				System.out.println(sum);
			}
		}
		//if(sum==0) sum=1;
		waitTime = avgTime * (queuelength/sum + 1/2);

		return waitTime; 
	}
}
