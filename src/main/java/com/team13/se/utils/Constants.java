package com.team13.se.utils;

public interface Constants {
	static final String ISSUE_TICKET = "/newTicket";
	static final String GET_LAST_TICKET = "/getLastTicket";
	enum requestType{shipping,accountManagement, payFees,payFines};
}
