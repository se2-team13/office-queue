var module = angular.module('home.controllers', []);

function myTime(time) {
    var hr = ~~(time / 3600);
    var min = ~~((time % 3600) / 60);
    var sec = time % 60;
    var sec_min = "";
    if (hr > 0) {
        sec_min += "" + hr + ":" + (min < 10 ? "0" : "");
    }
    sec_min += "" + min + ":" + (sec < 10 ? "0" : "");
    sec_min += "" + sec;
    return sec_min+ " min";
}

module.controller("HomeController", [ "$scope", "HomeService",
        function($scope, HomeService) {

                console.log("controller");
                $scope.ticketDto = {
                    ticketId : null ,
                    timestamp : null ,
                    requesttype : null,
                    waitingtime : null,
                    nInLine : null
                };
                

                $scope.newTicket = function(){
                    
                    switch ($scope.ticketDto.requesttype) {
                        case "shipping":
                            $scope.ticketDto.requesttype = 0;
                            break;
                        case 'accountMng':
                            $scope.ticketDto.requesttype = 1;
                            break;
                        case 'feesPayment':
                            $scope.ticketDto.requesttype = 2;
                        break;
                        case 'finesPayment':
                            $scope.ticketDto.requesttype = 3;
                        break;
                        default:
                            break;
                    }
                    $scope.ticketDto.waitingtime = null;
                    $scope.ticketDto.nInLine = null;
                    $scope.ticketDto.ticketId = null;
                    $scope.ticketDto.timestamp = null;
                    HomeService.newTicket($scope.ticketDto);
                    HomeService.getLastTicket().then(function(ticket){
                        $scope.ticketDto = ticket.data;
                        switch ($scope.ticketDto.requesttype) {
                            case 0:
                                $scope.ticketDto.requesttype = 'Shipping';
                                break;
                            case 1:
                                $scope.ticketDto.requesttype = 'Accoung Management';
                                break;
                            case 2:
                                $scope.ticketDto.requesttype = 'Pay Fees';
                                break;
                            case 3:
                                $scope.ticketDto.requesttype = 'Pay Fines';
                                break;
                            default:
                                break;
                        }
                        //fix waiting time
                        /*let tmp = myTime($scope.ticketDto.waitingtime);
                        $scope.ticketDto.waitingtime = tmp;*/
                        $scope.ticketDto.waitingtime = myTime(Math.round($scope.ticketDto.waitingtime));
                        $scope.ticketDto.timestamp = $scope.ticketDto.timestamp.slice(0,-2);
                    });
                    //window.location.replace("ticketInfo");
                }
        }]);