angular.module('home.services', []).factory('HomeService',
[   "$http", "CONSTANTS", function($http, CONSTANTS){
    var service={};

    service.newTicket = function(ticketDto){
        return $http.post(CONSTANTS.newTicket,ticketDto);
    }
    service.getLastTicket = function(){
        return $http.get(CONSTANTS.getLastTicket);
    }
    return service;
}
    
]);