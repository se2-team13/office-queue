'use strict'

var demoApp = angular.module('demo', [ 'home.controllers', 'home.services' ]);
   
demoApp.constant("CONSTANTS", {
    newTicket : "/ticket/newTicket",
    getLastTicket : "ticket/getLastTicket"
});